import numpy as np
import pandas as pd
from graphviz import Graph

dot = Graph("G", comment="Movies graph")
dot.graph_attr.update(rankdir="LR", ranksep="10", splines="line")

df = pd.read_csv(
    "data.csv",
    sep=";",
    encoding="latin-1",
    true_values=["Yes"],
    false_values=["No"],
    dtype={
        "Year": "Int16",
        "Length": "Int16",
        "Popularity": "Int8",
        "Awards": "Bool"
    }
)

print(df.values)

# number of nodes
n = len(df)

uncat_color = "grey"
colors = [
    "brown", # popularity < 10
    "crimson", # < 20
    "red2", # < 30
    "orangered", # < 40
    "orange", # < 50
    "yellow2", # < 60
    "yellowgreen", # < 70
    "chartreuse2", # < 80
    "limegreen", # < 90
    "green2", # >= 90
]

actor_prefix = "actor "
actor_prefix_len = len(actor_prefix)
director_prefix = "director "
director_prefix_len = len(director_prefix)
movie_prefix = "movie "
movie_prefix_len = len(movie_prefix)

actors = []
directors = []

movies_title = []
movies_color = []

for i in range(n):
    movie = df.iloc[i]
    movies_title.append((f"\"{movie_prefix}{movie['Title']}\"").replace(":", " -"))

    if not pd.isna(movie["Actor"]):
        # If main actor provided, save actor name
        actor = f"\"{actor_prefix}{movie['Actor']}\""
        if actor not in actors:
            actors.append(actor)

    if not pd.isna(movie["Actress"]):
        # If main actress provided, save actress name
        actress = f"\"{actor_prefix}{movie['Actress']}\""
        if actress not in actors:
            actors.append(actress)

    if not pd.isna(movie["Director"]):
        # If director provided, save director name
        director = f"\"{director_prefix}{movie['Director']}\""
        if director not in directors:
            directors.append(director)

with dot.subgraph(name='cluster_Directors') as sg:
    sg.attr(color="blue")
    sg.attr(label="Directors")

    for director in directors:
        sg.node(director, label=director[1+director_prefix_len:-1])

with dot.subgraph(name='cluster_Movies') as sg:
    sg.attr(color="red")
    sg.attr(label="Movies", nodesep="20")

    for i in range(n):
        movie = df.iloc[i]
        movie_popularity = movie["Popularity"]
        movie_title = movies_title[i]
        movies_title.append(movie_title)

        if pd.isna(movie_popularity):
            color = uncat_color
        else:
            color_indice = np.int_(movie_popularity / 10) if movie_popularity != 100 else len(colors) - 1
            color = colors[color_indice]
        movies_color.append(color)

        # Create the movie node
        sg.node(movie_title, color=color, label=movie_title[1+movie_prefix_len:-1])

with dot.subgraph(name='cluster_Actors') as sg:
    sg.attr(color="green")
    sg.attr(label="Main actors")

    for actor in actors:
        sg.node(actor, label=actor[1+actor_prefix_len:-1])

for i in range(n):
    print(f"{i+1}/{n}", end="\r", flush=True)
    movie = df.iloc[i]
    movie_title = movies_title[i]
    movie_color = movies_color[i]

    if not pd.isna(movie["Director"]):
        director = f"\"{director_prefix}{movie['Director']}\""
        # Create an edge between the movie and the director
        dot.edge(director, movie_title, color=movie_color)

    if not pd.isna(movie["Actor"]):
        actor = f"\"{actor_prefix}{movie['Actor']}\""
        # Create an edge between the movie and the main actor
        dot.edge(movie_title, actor, color=movie_color)

    if not pd.isna(movie["Actress"]):
        actress = f"\"{actor_prefix}{movie['Actress']}\""
        # Create an edge between the movie and the main actress
        dot.edge(movie_title, actress, color=movie_color)

# visualize the graph
print("\ncomputing...")
graph_name = "images/graph"
dot.render(graph_name)
dot.view()