import pandas as pd
import statistics

df = pd.read_csv(
    "data.csv",
    sep=";",
    encoding="latin-1",
    true_values=["Yes"],
    false_values=["No"],
    dtype={
        "Year": "Int16",
        "Length": "Int16",
        "Popularity": "Int8",
        "Awards": "Bool"
    }
)

persons = []
actors = []
directors = []
years = []

n_movies = len(df)

def put_in_array(array, name, popularity):
    if pd.isna(name):
        return

    item = [item for item in array if item["name"] == name]

    if len(item) == 0:
        item = {"name": name, "movies": [], "movies_pop": []}
        array.append(item)
    else:
        item = item[0]

    if not pd.isna(popularity):
        item["movies"].append(i)
        item["movies_pop"].append(popularity)

def calculate_average_pop(array):
    for item in array:
        if len(item["movies_pop"]) == 0:
            item["average_pop"] = -1
            item["median_pop"] = -1
            continue
        item["average_pop"] = int(round(sum(item["movies_pop"]) / len(item["movies_pop"])))
        item["median_pop"] = int(round(statistics.median(item["movies_pop"])))

def sort_array_by(array, key):
    return sorted(array, key=lambda i: i[key], reverse=True)

def save_csv(array, filename, key):
    with  open(f"output_clusters/{filename}_{key}.csv", "w") as f:
        for item in array:
            f.write(f"{str(item[key]) if item[key] >= 0 else ''};"
                    f"{item['name']};"
                    f"{','.join(str(n) for n in item['movies'])};"
                    f"{','.join(str(p) for p in item['movies_pop'])}\n")

def save_csv_average(array, filename):
    save_csv(sort_array_by(array, "average_pop"), filename, "average_pop")
def save_csv_median(array, filename):
    save_csv(sort_array_by(array, "median_pop"), filename, "median_pop")

# Saving data
for i in range(n_movies):
    print(f"{i + 1}/{n_movies}", end="\r", flush=True)
    movie = df.iloc[i]
    movie_pop = movie["Popularity"]

    put_in_array(persons, movie["Actor"], movie_pop)
    put_in_array(actors, movie["Actor"], movie_pop)

    put_in_array(persons, movie["Actress"], movie_pop)
    put_in_array(actors, movie["Actress"], movie_pop)

    put_in_array(persons, movie["Director"], movie_pop)
    put_in_array(directors, movie["Director"], movie_pop)

    put_in_array(years, movie["Year"], movie_pop)

print("\nCalculating averages and medians...")
calculate_average_pop(persons)
calculate_average_pop(actors)
calculate_average_pop(directors)
calculate_average_pop(years)

print("Sorting and saving by averages...")
save_csv_average(persons, "persons")
save_csv_average(actors, "actors")
save_csv_average(directors, "directors")
save_csv_average(years, "years")

print("Sorting and saving by medians...")
save_csv_median(persons, "persons")
save_csv_median(actors, "actors")
save_csv_median(directors, "directors")
save_csv_median(years, "years")

print("Finished!")